const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const { ObjectId } = mongoose.Schema;

const employerInfo = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    orgName:{        // organization name
        type:String,
        required:true
    },
    uniq:String,
    firstName: {
        type: String,
        required: true,
        length: {
            min: 3,
            max: 30
        },
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        length: {
            min: 3,
            max: 30
        },
        trim: true
    },
    phone: {
        type: String,
        required: true,
        length: 10,
        trim: true
    },
    address:{
        country:String,
        city:String
    },
    Discription: {
        type: String
    },
    website:{
        type:String
    },
    orgLogo: {       // organization log
        fileName:{
            type:String,
            trim:true
        },
        Path:{
            type:String,
            trim:true
        }
    },
    otherDocument: {    // for verification 
        fileName:{
            type:String,
            trim:true
        },
        Path:{
            type:String,
            trim:true
        }
    },
    social: {           // for verification 
        youtube: {
            type: String
        },
        facebook: {
            type: String
        },
        instagram: {
            type: String
        },
        linkIn:{
            type:String
        }
    }
    // ,
    // connections: [{
    //     onId:{
    //         type: Schema.Types.ObjectId,
    //         refPath:'onModel'
    //     },
    //     onModel:{
    //         type:String,
    //         enum:['hrDetail','userDetail']
    //     }
    // }]

}, { timestamps: true });

module.exports = hrDetailModel =  mongoose.model('employerProfile', employerInfo)