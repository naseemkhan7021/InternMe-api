const mongoose = require('mongoose');
const uuid = require('uuidv1');
const crypto = require('crypto');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true
    },
    salt: String,
    hash_password: {
        type: String,
        required: true,
        min: 6
    },
    token: String,       // for verification
    detail: {            //checke details are fill or not
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        default: 'user'
    }

}, { timestamps: true });


// Creat the vertual argumetns usint the vartual() 
userSchema.virtual('password')
    .set(function (password) {
        this._password = password
        this.salt = uuid();
        this.token = uuid();
        this.hash_password = this.encryptedPassword(password);
        // console.log('hash pass is  -> ',this.hash_password);
        // console.log('secret key is  -> ',process.env.SRK_KEY);
    })
    .get(function () {
        return this._password;
    })
userSchema.methods = {
    authenticat: function (plantext) {
        return this.encryptedPassword(plantext) === this.hash_password
    },
    encryptedPassword: function (password) {
        if (!password) return '';
        try {
            return crypto
                .createHash('sha1', this.salt)
                .update(password)
                .digest('hex')
        } catch (error) {
            return '';
        }
    }
}
module.exports  = mongoose.model('User', userSchema);