const usreDetails = require('../../../model/caditateModel/ragisterDetail')

//WORKROLE
// creat 
exports.workrole = async (req, res, next) => {
    /** creat workrole
        @type   POST
        @dec    user can creat workrole
        @rout   /workrol
        @access PRIVET
    */
    // await console.log('user id -> ', req.auth.payload.id2);
    console.log('body -> ',req.body);
    // return res.status(200).json({user:req.auth})
    if (req.body.role && req.body.company && req.body.country && req.body.from && req.body.details) {
        await usreDetails.findOne({ _id: req.auth.payload.id2 })
            .then(profile => {

                profile.workrole.unshift(req.body);
                profile
                    .save()
                    .then(profile => res.status(200).json({success:true,massage:'update Succefully !!'}))
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    } else {
        return res.status(417).json({ success: false, massage: 'All feilds are required !!' })
    } 
}

// delete
exports.workroleDelet = async (req, res) => {
    /** delet workrole
        @type   DELET
        @dec    user can delet workrole
        @rout   /workrol/:w_id
        @access PRIVET
    */
    await usreDetails.findOne({ _id: req.auth.payload.id2 })
        .then(profile => {
            //assignemnt to check if we got a profile
            const removethis = profile.workrole
                .map(item => item.id)
                .indexOf(req.params.w_id);
            // console.log('index -> ', removethis);
            profile.workrole.splice(removethis, 1);

            profile
                .save()
                .then(profile => res.status(200).json({success:true,massage:'Deleted Succefully !!'}))
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
}


//EDUCATION
// creat 
exports.education = async (req, res, next) => {
    /** creat educations
        @type   POST
        @dec    user can creat educations
        @rout   /education
        @access PRIVET
    */
    // await console.log('user id -> ', req.auth.payload.id2);

    // return res.status(200).json({user:req.auth})
    if (req.body.school && req.body.degree && req.body.fieldofstudy && req.body.start && req.body.end, req.body.grade) {

        await usreDetails.findOne({ _id: req.auth.payload.id2 })
            .then(profile => {
                profile.educations.unshift(req.body);
                profile
                    .save()
                    .then(profile => res.status(200).json({success:true,massage:'update Succefully !!'}))
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    } else {
        return res.status(417).json({ success: false, massage: 'All feilds are required !!' })
    }
}

// delete
exports.educationDelet = async (req, res) => {
    /** delet educations
        @type   DELET
        @dec    user can delet educations
        @rout   /workrol/:w_id
        @access PRIVET
    */
    await usreDetails.findOne({ _id: req.auth.payload.id2 })
        .then(profile => {
            //assignemnt to check if we got a profile
            const removethis = profile.educations
                .map(item => item.id)
                .indexOf(req.params.e_id);
            // console.log('index -> ', removethis);
            profile.educations.splice(removethis, 1);

            profile
                .save()
                .then(profile => res.status(200).json({success:true,massage:'Deleted Succefully !!'}))
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
}
//Skills
// creat 
exports.skill = async (req, res, next) => {
    /** creat skills
        @type   POST
        @dec    user can creat skills
        @rout   /skill
        @access PRIVET
    */
    // await console.log('user id -> ', req.auth.payload.id2);

    // return res.status(200).json({user:req.auth})
    if (req.body.name && req.body.level) {

        await usreDetails.findOne({ _id: req.auth.payload.id2 })
            .then(profile => {
                profile.skills.unshift(req.body);
                profile
                    .save()
                    .then(profile => res.status(200).json({success:true,massage:'update Succefully !!'}))
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    } else {
        return res.status(417).json({ success: false, massage: 'All feilds are required !!' })
    }
}

// delete
exports.skillDelet = async (req, res) => {
    /** delet skills
        @type   DELET
        @dec    user can delet skills
        @rout   /workrol/:w_id
        @access PRIVET
    */
    await usreDetails.findOne({ _id: req.auth.payload.id2 })
        .then(profile => {
            //assignemnt to check if we got a profile
            const removethis = profile.skills
                .map(item => item.id)
                .indexOf(req.params.s_id);
            // console.log('index -> ', removethis);
            profile.skills.splice(removethis, 1);

            profile
                .save()
                .then(profile => res.status(200).json({success:true,massage:'Deleted Succefully !!'}))
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
}