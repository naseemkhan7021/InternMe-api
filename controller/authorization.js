const User = require('../model/person') // this model is for edmin, student and employer
const userDetailModel = require('../model/caditateModel/ragisterDetail')
const employerDetailModel = require('../model/hrModel/ragisterDetail')
const jwt = require('jsonwebtoken');
const ejwt = require('express-jwt');
require('dotenv').config();

//singup 
exports.singup = async (req, res) => {
    // console.log('data -> ',req.body)
    let userType = req.params.userType
    // console.log('userType -> ', userType);
    let existEmail = await User.findOne({ email: req.body.email })
    if (existEmail) {
        return res.status(400).json({ error: `Email is already use by ${existEmail.role} !! try another and valid email` })
    };
    const newUser = await new User(req.body);
    newUser.role = userType
    await newUser.save();
    res.status(200).json({
        id: newUser._id,
        token: newUser.token,
        sucess: 'Successfullly created account go to login'
    });
};

//login
exports.login = async (req, res, next) => {
    // console.log('body data -> ', req.body)

    if (req.body.email && req.body.password) {
        const { email, password } = req.body

        User.findOne({ email }, (error, user) => {
            if (error || !user) {
                return res.status(404).json({ error: 'You are not ragister with this email pleas go to singup' })
            }
            // console.log('user -> ', user)
            if (!user.authenticat(password)) {
                return res.status(401).json({ error: "Email or password doesn't match" })
            }


            // if user not fill his detail then 
            if (!user.detail) {
                return res.status(402).json({ notDetail: true,role:user.role,id: user._id, token: user.token, error: 'Pleas fill all detail first !!' })
            }
            try {
                let userType = req.params.userType
                // console.log('usertype -> ',userType)
                let userDetail;
                if(userType === 'student'){
                    userDetail = userDetailModel.findOne({ user: user._id })
                }
                if(userType === 'employer'){
                    userDetail = employerDetailModel.findOne({ user: user._id })
                }
                // console.log('userdetail -> ',userDetail)
                // find full user detail for info 
                userDetail.populate('user').exec((error, userinfo) => {
                    if (error) {
                        // console.log('error on detial ->', error)
                        return res.status(402).json({ notDetail: true, id: user._id, token: user.toke, error: 'Pleas fill all detail first --!!' })
                    } else {
                        // console.log('user -> ',userinfo)
                        // generate a token with user id and secret
                        // generating the token for give this token to the login client 
                        const payload = {
                            id: userinfo.user._id,
                            id2: userinfo._id,
                            firstName: userinfo.firstName,
                            lastName: userinfo.lastName,
                            email: userinfo.user.email,
                            token: userinfo.user.token,
                            salt: userinfo.user.salt
                        }
                        const jsonwebtoken = jwt.sign({ payload }, process.env.SRK_KEY)
                        // console.log('token is -> ', jsonwebtoken)

                        // creat the cookie 
                        res.cookie('token', jsonwebtoken, { expire: new Date + 9999 })
                        // console.log('user all detail -> ', userinfo);

                        return res.status(200).json({ tokend: userinfo.user._id, role: userinfo.user.role, jwtoken: jsonwebtoken, token: userinfo.user.token })
                    }
                })
                //set the user detail for the client
            } catch (error) {
                // console.log('error catch -> ', error);
            }
 
        })
    } else {
        res.status(417).json({ error: 'pleas fill email and password feilds' })
    }
}
// Authorization
exports.isLogin = ejwt({
    // if the token is valid , express-jwt appends the verified users id
    // in an auth th  key to the request object
    secret: process.env.SRK_KEY,
    algorithms: ['HS256'],
    userProperty: 'auth'
});

// logout 
exports.logout = (req, res) => {
    res.clearCookie('token')
    res.status(200).json({
        success: true, massage: 'success to logout !!'
    })
}



exports.userInfo = async (req, res, next, id) => {
    // console.log('id - ', id);
    // let token = await req.params.token

    User.findById(id)
        .exec((error, user) => {
            // console.log('line 7 => ', user)
            if (error || !user) {
                return res.status(404).json({ error: 'User not found --- !!' })
            }
            // console.log('user -> ',user);
            req.userInfo = user  // add all admin info in adminprofile obj
            next();
        })
}

exports.checkUser = async (req, res, next) => {

    let exist = await User.findOne({ token: req.params.token, _id: req.params.id })
    if (exist) {
        if (req.userInfo.detail && req.userInfo.detail === true) {
            return res.status(202).json({ detail: true,role:req.userInfo.role })
        }
        if (req.userInfo.detail && req.userInfo.detail === false) {
            return res.status(202).json({ detail: false, massage: 'please fill detial first' })
        }
        return res.status(200).json({ success: true,role:req.userInfo.role })
    } else {
        return res.status(404).json({ error: 'User Not Found !!' })
    }
    next()
}