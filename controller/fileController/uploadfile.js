const fs = require('fs');
// const userDetailModel = require('../model/caditateModel/ragisterDetail');
const { storageConfig } = require('./fileConfig');
const userSingupModel = require('../../model/person');



// post or profile , employer and student  same update
exports.uploadFile = async (req, res, next) => {
    // console.log('this is upload file')
    // console.log('file data  line 8 -> ',req.data) 
    let filetype = req.params.filetype; // filetype is userprofile, userdoc, usercover <-- only 
    let profileOrpost = req.params.profileorpost // user is posting or for profile  -value only => profile/posts
    console.log('post or profile -> ', profileOrpost); //
    // console.log('token -> ', req.params.token, ' id -> ', req.params.id);
    let exist = await userSingupModel.findOne({ token: req.params.token, _id: req.params.id })
    console.log('exit line 12 -> ', exist);
    let file;
    try {

        if (exist) {
            console.log('emp or stud -> ',exist.role)  // -> exist.role is only employer or student
            if(exist.role === 'student'){
                if (filetype === 'profile' || 'cover') {
                    // for uploading the file storageConfig is user difinde function 
                    file = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.jpg', '.png', '.gif', '.jpeg'])
                }
                if (filetype === 'doc') {
                    file = await storageConfig(`${filetype}`, `${process.env.DOC_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.pdf', '.doc', '.docx', '.odt'])
                }
                if (filetype === 'video') {
                    file = await storageConfig(`${filetype}`, `${process.env.VIDEO_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.mp4', '.mov', '.avi', '.wmv'])
                    // .mp4, .mov, .avi and .wmv
                }
            }else{
                if (filetype === 'orgLogo') {
                    // for uploading the file storageConfig is user difinde function 
                    file = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.jpg', '.png', '.gif', '.jpeg'])
                }
                if (filetype === 'otherDocument') {
                    file = await storageConfig(`${filetype}`, `${process.env.DOC_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.pdf', '.doc', '.docx', '.odt'])
                }
            }
            
            // // for uploading the file storageConfig is user difinde function 
            // let candidateImg = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/profile/`, ['.jpg', '.png', '.gif', '.jpeg'])

            try {
                file(req, res, error => {
                    if (error) {
                        return res.status(415).json({ error: req.fileValidationError }) // come form express 
                    }
                    else {
                        console.log('file info is => ', req.file)
                        return res.status(200).json({
                            // fileinfo: req.file
                            fileName: req.file.filename,
                            filePath: req.file.path
                        })
                    };
                });
            } catch (error) {
                console.log('error catch line 52 -> ', error);
                res.status(401).json({ error: 'catch line 55 the error' })
            }

        } else {
            return res.status(404).json({ error: 'User not found !!' })
        }
    } catch (error) {
        console.log('somthin is error line 60 -> ', error);
        return res.status(403).json({ error: 'somthing is error pleas try again !!' })
    }
}
