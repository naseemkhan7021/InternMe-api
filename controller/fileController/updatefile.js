const fs = require('fs');
const studentDetailModel = require('../../model/caditateModel/ragisterDetail');
const employerDetailModel = require('../../model/hrModel/ragisterDetail');
const { storageConfig } = require('./fileConfig');
const userSingupModel = require('../../model/person');

// post or profile , employer and student  same update
exports.updatefile = async (req, res, next) => {
    // console.log('this is upload file')
    // console.log('file data  line 8 -> ',req.data) 
    let filetype = req.params.filetype; // this is front form fieldName filetype is userprofile, userdoc, usercover <-- only 
    let profileOrpost = req.params.profileorpost // user is posting or for profile  -value only => profile/posts
    console.log('post or profile -> ', profileOrpost); //
    // console.log('token -> ', req.params.token, ' id -> ', req.params.id);
    let exist = await userSingupModel.findOne({ token: req.params.token, _id: req.params.id })
    // console.log('exit line 70 -> ', exist);
    let file;
    let profilepath;
    try {

        if (exist) {
            // cvResume , profileCoverimg, profileImg
            if (exist.role === 'student') {
                profilepath = await studentDetailModel.findOne({ user: exist._id }).select('profileImg cvResume profileCoverimg')

                // save file for student
                if (filetype === 'profile' || 'cover') {
                    if (filetype === 'profile' && profilepath.profileImg.Path) {
                        console.log('prfile -> ', profilepath.profileImg.Path);
                        fs.unlink(profilepath.profileImg.Path, () => {
                            console.log('delete file -> ', profilepath.profileImg.Path);

                        })
                    } else if (filetype === 'cover' && profilepath.profileCoverimg.Path) {
                        console.log('cover -> ', profilepath.profileCoverimg.Path);
                        fs.unlink(profilepath.profileCoverimg.Path, () => {
                            console.log('delete file -> ', profilepath.profileCoverimg.Path);

                        })
                    }
                    // for uploading the file storageConfig is user difinde function 
                    file = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.jpg', '.png', '.gif', '.jpeg'])
                }


                if (filetype === 'doc') {
                    console.log('cover -> ', profilepath.cvResume);
                    if (profilepath.cvResume.Path) {
                        console.log('cover -> ', profilepath.cvResume.Path);
                        fs.unlink(profilepath.cvResume.Path, () => {
                            console.log('delete file -> ', profilepath.cvResume.Path);
                        })
                    }
                    file = await storageConfig(`${filetype}`, `${process.env.DOC_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.pdf', '.doc', '.docx', '.odt'])
                }
                if (filetype === 'video') {
                    file = await storageConfig(`${filetype}`, `${process.env.VIDEO_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.mp4', '.mov', '.avi', '.wmv'])
                    // .mp4, .mov, .avi and .wmv
                }

            } else {
                profilepath = await employerDetailModel.findOne({ user: exist._id }).select('orgLogo otherDocument')
                if (filetype = 'orglog') { // orglog come from frant end params 
                    console.log('cover -> ', profilepath.orgLogo.Path);

                    fs.unlink(profilepath.orgLogo.Path, () => {
                        console.log('delete file -> ', profilepath.orgLogo.Path);

                    })

                    // for uploading the file storageConfig is user difinde function 
                    file = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.jpg', '.png', '.gif', '.jpeg'])
                }
                if(filetype = 'doc'){ // come from frant end params
                    console.log('cover -> ', profilepath.otherDocument.Path);

                    fs.unlink(profilepath.otherDocument.Path, () => {
                        console.log('delete file -> ', profilepath.otherDocument.Path);

                    })

                    // for uploading the file storageConfig is user difinde function 
                    file = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/${exist.role}/${profileOrpost}/`, ['.jpg', '.png', '.gif', '.jpeg'])
                }

            }

            // // for uploading the file storageConfig is user difinde function 
            // let candidateImg = await storageConfig(`${filetype}`, `${process.env.IMG_FILE_LOCATION}/profile/`, ['.jpg', '.png', '.gif', '.jpeg'])

            try {
                file(req, res, error => {
                    if (error) {
                        return res.status(415).json({ error: req.fileValidationError }) // come form express 
                    }
                    else {
                        console.log('file info is => ', req.file)
                        return res.status(200).json({
                            // fileinfo: req.file
                            fileName: req.file.filename,
                            filePath: req.file.path
                        })
                    };
                });
            } catch (error) {
                console.log('error catch line 52 -> ', error);
                res.status(401).json({ error: 'catch line 55 the error' })
            }

        } else {
            return res.status(404).json({ error: 'User not found !!' })
        }
    } catch (error) {
        console.log('somthin is error line 60 -> ', error);
        return res.status(403).json({ error: 'somthing is error pleas try again !!' })
    }
}

