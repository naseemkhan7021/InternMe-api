const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
require('dotenv').config();
const port = process.env.PORT || 8080;
console.log('the port is -> ', port);
const app = express();

// database connections 
mongoose.connect(process.env.MONGO_URI, { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => {
        console.log('DB connected !!');
    })
    .catch(e => {
        console.log('DB not connecte pleas try again -> ', e)
    })
// mongoose.connection.on('error',error => {
//     console.log('Db not connecte please try again !! ->',error);
// })

 

// import route middelware
// authentication rout 
const Authrouter = require('./route/authorization');
const { isLogin } = require('./controller/authorization');
// details route (create profile,post for employer and student)
const rootRoute = require('./route/mainroot');
const stdprofile = require('./route/userRoute/profile')
// const hrAuthrouter = require('./route/hrRoute/authorization');
const moreDetail = require('./route/userRoute/MoreDetail')
const empProfile = require('./route/hrRoute/profile');
// const hrmoreDetail = require('./route/hrRoute/MoreDetail')

// import static files
app.use(express.static('public'));

// use all middelwere
//module
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors()); 
//route
app.use(rootRoute);
app.use('/api/userAuth', Authrouter) 
app.use('/api/userProfile', bodyParser.json(), stdprofile) //std -> student
app.use('/api/moreDetail', isLogin, moreDetail)
// app.use('/api/hrAuth', bodyParser.json(), hrAuthrouter)
app.use('/api/employerProfile', empProfile)                // emp -> employer
// app.use('/api/hrmoreDetail', isLogin,hrmoreDetail )


// Authorizetion erro handle 
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        return res.status(401).json({ 
            massage: 'Please signin you are not authorized'
        });
    } 
});

app.listen(port, () => { 
    console.log(`copy this URL -> localhost:${port}`);
});